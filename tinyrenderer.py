from PIL import Image
import numpy as np

def output_image(buffer: np.array):
    '''
    This function will transform the numpy array into an BITMAP called
    output_image.BMP

    args:
        buffer: numpy array size (n,m,3), three channels of n x m size.
    '''


    #flip axis to match my reference frame.
    nbuffer = np.flip(np.transpose(buffer,axes=(1,0,2)),axis=0)

    # Convert the NumPy array to an image
    image = Image.fromarray(nbuffer)

    # Save the image to disk storage
    image.save('output_image.bmp', format="bmp")

    # Close the image
    image.close()

def put_pixel(buffer, coord: tuple, color: tuple):
    buffer[coord[0],coord[1],:] = np.array(color, dtype=np.uint8)

def draw_line(buffer: np.array, a: tuple, b: tuple, color: tuple = (255,255,255) ):
    '''
    Draw a line using Bresenham's line algorithm.

    x = (1-t)*a_x + t*b_x
    y = (1-t)*a_y + t*b_y

    args:
        buffer: numpy array of ints where data is saved.
        a: tuple of initial point.
        b: tuple of the final point
    optional:
        color: a tuple that holds the RGB for the color of the line.
    '''

    ax = a[0]
    ay = a[1]
    bx = b[0]
    by= b[1]
    


    if by-ay > bx-ax:
        
        if ay > by:
            ax,bx = bx,ax
            ay,by = by,ay

        ys = np.arange(ay,by,1,dtype=int)

        for y in ys:

            t = (y-ay)/(by-ay)


            # calculate the coordinates
            x = int(np.round((1-t)*ax + t*bx))
            #y = int(np.round((1-t)*ay + t*by))

            put_pixel(buffer,(x,y), color)
    else:
       
        if ax > bx:
            ax,bx = bx,ax
            ay,by = by,ay

        xs = np.arange(ax,bx,1,dtype=int)

        for x in xs:

            t = (x-ax)/(bx-ax)


            # calculate the coordinates
            #x = int(np.round((1-t)*a[0] + t*b[0]))
            y = int(np.round((1-t)*ay + t*by))

            put_pixel(buffer,(x,y), color)

def create_buffer(width:int, height: int):
    '''
    Function that created a numpy array to be used as a buffer with 3 color channels.

    args:
        width: width of the image.
        height: height of the image.

    returns:
        buffer: a array of width by height by 3 of uint8.
    '''

    return np.zeros((width,height,3),dtype=np.uint8)


def find_bounding_box(a:tuple, b:tuple , c:tuple):
    '''
    find the bounding box of a triangle. the bottom left point and the upper right point.

    args:
        a: vertex 1, tuple of int, x and y.
        b: vertex 2, tuple of int, x and y,
        c: vertex 3, tuple of int, x and y,

    returns:
        l,r: the bottom left point and the upper right point
    '''

    most_left_in_x = min([a,b,c],key = lambda v: v[0])
    most_bottom_in_y = min([a,b,c],key = lambda v: v[1])

    l = (most_left_in_x[0],most_bottom_in_y[1])

    most_right_in_x = max([a,b,c],key = lambda v: v[0])
    most_top_in_y = max([a,b,c],key = lambda v: v[1])

    r = (most_right_in_x[0],most_top_in_y[1])

    return l,r

def calculate_area(a:tuple, b:tuple , c:tuple):
    '''
    calculate area of a triangle. 

    args
        a: vertice A of the triangle.
        b: vertice B of the triangle.
        c: vertice C of the triangle.

    returns
        area: total area of the triangle. 
    '''



    d = (a[0] + c[0]-b[0], a[1] + c[1]-b[1])

    # https://en.wikipedia.org/wiki/Determinant
    A = b[0]-a[0]
    B = b[1]-a[1]

    C = d[0]-a[0]
    D = d[1]-a[1] 

    return (A*D-B*C)/2


def is_inside_triangle(coord:tuple, a:tuple, b:tuple , c:tuple):
    '''
    check if point is inside triangle. if it is, returns true.
    uses the barycentric coordinates. if any are negative, it means
    that coord is outside.
     
    args:
        coord: point to check if it is inside.
        a: vertice A of the triangle.
        b: vertice B of the triangle.
        c: vertice C of the triangle.
    '''
    total_area = calculate_area(a,b,c)

    P1 = calculate_area(coord,b,c)/total_area

    if P1 < 0:
        return False

    P2 = calculate_area(a,coord,c)/total_area
    
    if P2 < 0:
        return False
    
    P3 = calculate_area(a,b,coord)/total_area
    
    if P3 < 0:
        return False
    
    return True

def triangle(buffer: np.array, a:tuple, b:tuple , c:tuple , color:tuple = (255,255,255)):
    '''
    Draws a full color triangle

    args:
        buffer: buffer to draw the triangle in, numpy array of with 3 channels.
        a: vertex 1, tuple of int, x and y.
        b: vertex 2, tuple of int, x and y,
        c: vertex 3, tuple of int, x and y,
        color (optional): color, tuple of 3 ints, default is white.
    '''

    # get bounding box of triangle
    l,r = find_bounding_box(a,b,c)

    xs = np.arange(l[0],r[0],dtype=int)
    ys = np.arange(l[1],r[1],dtype=int)

    for x in xs:
        for y in ys:
            if is_inside_triangle((x,y),a,b,c):
                put_pixel(buffer,(x,y),color)
    
    