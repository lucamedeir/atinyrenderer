import numpy as np
import pywavefront
from tinyrenderer import *

diablo = pywavefront.Wavefront('objs/diablo3_pose.obj',collect_faces=True)

width = 700
height =700
buffer = np.zeros((width,height,3), dtype=np.uint8)

red = (255,0,0)


for face in diablo.mesh_list[0].faces:

    v1 = diablo.vertices[face[0]]
    v2 = diablo.vertices[face[1]]
    v3 = diablo.vertices[face[2]]

        # scale them, ignore z
    a = np.array([ (v1[0]+1)*width/2, (v1[1]+1)*height/2], dtype=int)-1
    b = np.array([ (v2[0]+1)*width/2, (v2[1]+1)*height/2], dtype=int)-1
    c = np.array([ (v3[0]+1)*width/2, (v3[1]+1)*height/2], dtype=int)-1


    assert a[0] < width
    assert a[1] < height
    assert b[0] < width
    assert b[1] < height
    assert c[0] < width
    assert c[1] < height

    draw_line(buffer, a, b, red)
    draw_line(buffer, b, c, red)
    draw_line(buffer, c, a, red)

output_image(buffer)